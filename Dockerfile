FROM golang:1.16.6-alpine as builder

WORKDIR /build

COPY . .

RUN ls -la

ARG GOPRIVATE
ARG GIT_USER
ARG GIT_PASSWORD
RUN if [ ! -z "$GIT_USER" ] && [ ! -z "$GIT_PASSWORD" ]; then \
        printf "machine gitlab.com\n \
            login ${GIT_USER}\n \
            password ${GIT_PASSWORD}\n" \
            >> ${HOME}/.netrc;\
    fi

RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build -o bin/server cmd/main.go

FROM alpine

WORKDIR /app
COPY --from=builder /build/bin/server .

ENTRYPOINT ["/app/server"]
