package pkg

import (
	"context"
	"fmt"
	"math/rand"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/pow4wow/common"
)

// Listener default values
const (
	defaultHost = "0.0.0.0" // to be able to run in Docker
	defaultPort = "9091"

	defaultConnectionTimeOut = time.Second // Incoming connection lifetime
)

type (
	// Listener is main object in whole project )
	Listener struct {
		port             string
		nonce            []byte
		banned           map[string]struct{}
		connectorFactory ConnectorFactory
	}
)

// Serve starts tcp listener
func (l Listener) Serve() {
	addr := net.JoinHostPort(defaultHost, l.port)
	srv, err := net.Listen("tcp", addr)
	if err != nil {
		fmt.Println(err)
		return
	}

	defer func() {
		if err := srv.Close(); err != nil {
			fmt.Printf("can't shutdown listener: %v\nexiting...\n", err)
			return
		}
	}()

	signalChan := make(chan os.Signal)
	signal.Notify(signalChan, os.Interrupt, syscall.SIGTERM)

	// running listener in separated routine
	go func(srv net.Listener) {
		for {
			conn, err := srv.Accept()
			fmt.Printf("received connection from %s\n", conn.RemoteAddr())
			if err != nil {
				return
			}

			// check if client address is banned
			if _, ok := l.banned[conn.RemoteAddr().String()]; ok {
				fmt.Printf("%s banned\n", conn.RemoteAddr())
				continue
			}

			// handling incoming connection (ORLY)
			go l.handleConnection(conn)
		}
	}(srv)

	// waiting for os signals to terminate process
	<-signalChan
}

// NewListener returns listener instance
func NewListener(port string, connectorFactory ConnectorFactory) (*Listener, error) {
	if connectorFactory == nil {
		return nil, fmt.Errorf("connector factory can't be nil")
	}

	if port == "" {
		port = defaultPort
	}

	nonce := make([]byte, defaultNonceLength)

	_, err := rand.Read(nonce)
	if err != nil {
		return nil, err
	}

	return &Listener{
		port:             port,
		nonce:            nonce,
		banned:           make(map[string]struct{}),
		connectorFactory: connectorFactory,
	}, nil
}

func (l Listener) handleConnection(conn net.Conn) {
	defer func() {
		if err := conn.Close(); err != nil {
			fmt.Printf("can't close connection: %v\nexiting...", err)
			return
		}
	}()

	// creating connector instance for current connection
	connectorInstance, err := l.connectorFactory(conn)
	if err != nil {
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), defaultConnectionTimeOut)

	defer cancel()

	go common.WaitForMessage(
		ctx,
		conn,
		connectorInstance.IncomingChannel(),
		connectorInstance.ErrorChannel(),
		connectorInstance.StopChannel(),
	)
	go common.WriteToChannel(
		ctx,
		conn,
		connectorInstance.OutgoingChannel(),
		connectorInstance.ErrorChannel(),
		connectorInstance.StopChannel(),
	)

	for {
		select {
		case msg := <-connectorInstance.IncomingChannel():
			fmt.Printf("received message: %s\n", msg)
			go connectorInstance.ProcessIncomingMessage(msg)
		case err := <-connectorInstance.ErrorChannel():
			l.banned[conn.RemoteAddr().String()] = struct{}{}
			fmt.Println(err)

			return
		case <-connectorInstance.StopChannel():
			fmt.Printf("aborting connection %s\n", conn.RemoteAddr())
			return
		}
	}
}
