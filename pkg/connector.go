package pkg

import (
	"math/rand"
	"net"

	"github.com/google/uuid"
	"gitlab.com/pow4wow/common"
	"gitlab.com/pow4wow/pow"
	"gitlab.com/pow4wow/wow"
)

const (
	defaultNonceLength = 32
)

type (
	Connector interface {
		// ProcessIncomingMessage should work with incoming messages form client
		// Inside this method happens some magic that implements PoW for client
		ProcessIncomingMessage(msg []byte)
		IncomingChannel() chan []byte
		OutgoingChannel() chan []byte
		ErrorChannel() chan error
		StopChannel() chan struct{}
		// Cancel should stop any connector activity
		Cancel() error
	}

	// ConnectorFactory is Connector constructor used to create separate instance for each incoming connection
	ConnectorFactory func(conn net.Conn) (Connector, error)

	connector struct {
		conn         net.Conn
		pow          pow.Prover
		incomingChan chan []byte
		outgoingChan chan []byte
		errChan      chan error
		stopChan     chan struct{}
		boundData    string
	}
)

// ProcessIncomingMessage accepts message bytes (msg) and make decision what to do with it according to its content
func (c *connector) ProcessIncomingMessage(msg []byte) {
	// separating incoming message for header and its body
	header, message, err := common.GetMessageValues(msg)
	if err != nil {
		c.errChan <- err
		return
	}

	// decide what action we need to perform according to header value
	switch header {
	case common.ClientRequestServiceHeader:
		// generating task for client
		payload, err := c.pow.Generate()
		if err != nil {
			c.errChan <- err
			return
		}

		// preparing outgoing message
		outgoingMessage, err := common.PrepareMessage(common.ServerChallengeProvidedHeader, payload, c.boundData)
		if err != nil {
			c.errChan <- err
			return
		}

		// sending message back to client
		c.outgoingChan <- outgoingMessage
	case common.ClientChallengeResponseHeader:
		// validating clients work result
		if err := c.pow.Validate(message); err != nil {
			c.errChan <- err
			return
		}

		// getting what client needs
		wiz := wow.NewWisdomizer()
		word := wiz.GetWord()

		outgoingMessage, err := common.PrepareMessage(common.ServerWordOfWisdomResponse, word)
		if err != nil {
			c.errChan <- err
			return
		}

		// sending portion of wisdom back to client
		c.outgoingChan <- outgoingMessage

		// tell connector that it's time to close connection
		//c.stopChan <- struct{}{}
	}
}

func (c *connector) IncomingChannel() chan []byte {
	return c.incomingChan
}

func (c *connector) OutgoingChannel() chan []byte {
	return c.outgoingChan
}

func (c *connector) ErrorChannel() chan error {
	return c.errChan
}

func (c *connector) StopChannel() chan struct{} {
	return c.stopChan
}

func (c *connector) Cancel() error {
	return c.conn.Close()
}

// NewConnector is a Connector instance constructor
func NewConnector(conn net.Conn) (Connector, error) {
	nonce := make([]byte, defaultNonceLength)

	_, err := rand.Read(nonce)
	if err != nil {
		return nil, err
	}

	boundData := uuid.New().String()
	prover := pow.NewProver(pow.WithDifficulty(10), pow.WithBoundData([]byte(boundData)), pow.WithNonce(nonce))

	return &connector{
		conn:         conn,
		pow:          prover,
		boundData:    boundData,
		incomingChan: make(chan []byte),
		outgoingChan: make(chan []byte),
		errChan:      make(chan error),
		stopChan:     make(chan struct{}),
	}, nil
}
