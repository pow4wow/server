package pkg_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/pow4wow/server/pkg"
)

func TestNewListener(t *testing.T) {
	type tCase struct {
		name             string
		connectorFactory pkg.ConnectorFactory
		expectError      bool
	}

	var cases = []tCase{
		{
			name:             "OK",
			connectorFactory: pkg.NewConnector,
			expectError:      false,
		},
		{
			name:             "NOT OK",
			connectorFactory: nil,
			expectError:      true,
		},
	}

	caseRunner := func(c tCase) func(t *testing.T) {
		return func(t *testing.T) {
			listener, err := pkg.NewListener("", c.connectorFactory)

			if c.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
			assert.NotNil(t, listener)
		}
	}

	for _, cc := range cases {
		t.Run(cc.name, caseRunner(cc))
	}
}
