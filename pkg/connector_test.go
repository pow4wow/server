package pkg

import (
	"context"
	"fmt"
	"net"
	"os"
	"regexp"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/pow4wow/common"
	"golang.org/x/net/nettest"
)

var (
	srv net.Listener
	cnt Connector
)

func TestMain(m *testing.M) {
	var err error
	srv, err = nettest.NewLocalListener("tcp")
	if err != nil {
		panic(err)
	}

	defer func() {
		if err := srv.Close(); err != nil {
			fmt.Printf("can't stop listener: %v\n", err)
			panic(err)
		}

		if cnt != nil {
			if err := cnt.Cancel(); err != nil {
				fmt.Printf("can't stop connector: %v\n", err)
				panic(err)
			}
		}
	}()

	go func() {
		for {
			_, err := srv.Accept()
			if err != nil {
				fmt.Printf("can't accept incoming connection: %v\n", err)
				return
			}
		}
	}()

	os.Exit(m.Run())
}

func TestNewConnector(t *testing.T) {
	conn, err := net.Dial("tcp", srv.Addr().String())
	if !assert.NoError(t, err) {
		return
	}

	cnt, err = NewConnector(conn)
	assert.NoError(t, err)
	assert.NotNil(t, cnt)
}

func TestConnector_ProcessIncomingMessage(t *testing.T) {
	type testCase struct {
		name        string
		message     []byte
		expectError bool
		expected    *regexp.Regexp
	}

	var testCases = []testCase{
		{
			name:        "OK: request-service message",
			message:     []byte(fmt.Sprintf("%s:%s", common.ClientRequestServiceHeader, "")),
			expectError: false,
			expected:    regexp.MustCompile(fmt.Sprintf("^%s:", common.ServerChallengeProvidedHeader)),
		},
		{
			name:        "MUST FAIL: request-service message",
			message:     []byte(fmt.Sprintf("%s:%s", "invalid-request-service-header", "")),
			expectError: true,
			expected:    nil,
		},
	}

	testCaseRunner := func(c testCase) func(t *testing.T) {
		return func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), time.Second)
			defer cancel()

			go cnt.ProcessIncomingMessage(c.message)

			if c.expectError {
				readErrChan(ctx, t, cnt.ErrorChannel())
				return
			}

			readOutgoingChan(ctx, t, cnt.OutgoingChannel(), c.expected)

		}
	}

	for _, c := range testCases {
		t.Run(c.name, testCaseRunner(c))
	}
}

func readErrChan(ctx context.Context, t *testing.T, ch chan error) {
	for {
		select {
		case <-ctx.Done():
			t.Error("no error was returned")
			return
		case err := <-ch:
			assert.Error(t, err)
			return
		}
	}
}

func readOutgoingChan(ctx context.Context, t *testing.T, ch chan []byte, expected *regexp.Regexp) {
	for {
		select {
		case <-ctx.Done():
			t.Error("no message received")
			return
		case msg := <-ch:
			assert.Regexp(t, expected, string(msg))
			return
		}
	}
}
