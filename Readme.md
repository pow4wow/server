## What is this for?

You are looking at TCP server protected by PoW.

### How does it work?

It accepts connections and starts to listen for incoming messages.
Once it received it checks if message has appropriate format:
`{header}:{value1}:{value2}:...:{valueN}`
According to header part it decides what to do next.

In case of `request-service` header it returns to client some generated stuff.
Client should make some work with it and send next message with 
`challenge-response` header, job result payload and bound data used in job

Server validates it and answer with some wise quote (if exists)

### How to build image:
```
make docker-build 
```

### How to test it:

```
make test
```

### How build it:

```
make build
```

### How to run it:

After you've built it, just launch the binary
```
./bin/server {port}
```

it accepts exactly one optional argument - port to listen on. 
Default value for port is `9091`