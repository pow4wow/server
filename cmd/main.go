package main

import (
	"os"

	"gitlab.com/pow4wow/server/pkg"
)

func main() {
	port := ""
	if len(os.Args) > 1 {
		port = os.Args[1]
	}

	listener, err := pkg.NewListener(port, pkg.NewConnector)
	if err != nil {
		panic(err)
	}

	listener.Serve()
}
