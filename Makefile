BIN_NAME = "inventory"

SERVER_DOCKER_IMAGE_NAME = "pow4wow-server"
DOCKER_IMAGE_TAG = "latest"

#GOPRIVATE ?= "gitlab.com/pow4wow/*"

.PHONY: docker-build
docker-build:
	docker build -t $(SERVER_DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG) --build-arg GOPRIVATE=$(GOPRIVATE) --build-arg GIT_USER=$(GIT_USER) --build-arg GIT_PASSWORD=$(GIT_PASSWORD) .

.PHONY: deps
deps:
	go mod download

.PHONY: test
test: deps
	go test -v ./...

.PHONY: build
build: deps
	GO111MODULE=on go build -o bin/server cmd/main.go