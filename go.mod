module gitlab.com/pow4wow/server

go 1.16

require (
	github.com/google/uuid v1.3.0
	github.com/stretchr/testify v1.7.0
	gitlab.com/pow4wow/common v0.0.3
	gitlab.com/pow4wow/pow v0.0.1
	gitlab.com/pow4wow/wow v0.0.1
	golang.org/x/net v0.0.0-20210902165921-8d991716f632
)
